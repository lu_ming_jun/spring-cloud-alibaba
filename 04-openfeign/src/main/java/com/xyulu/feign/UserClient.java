package com.xyulu.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author luming
 * @version 1.0
 * @date
 */
@FeignClient("userservice")
public interface UserClient {

    @GetMapping("/user/remote")
    String testRemote();
}
