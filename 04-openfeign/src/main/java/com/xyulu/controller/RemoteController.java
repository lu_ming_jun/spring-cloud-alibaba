package com.xyulu.controller;

import com.xyulu.feign.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author luming
 * @version 1.0
 * @date
 */
@RestController
public class RemoteController {

    @Autowired
    private UserClient userClient;
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/remote")
    public String demo() {
//        return restTemplate.getForObject("http://userservice/user/remote", String.class);
        return userClient.testRemote();
    }

}
