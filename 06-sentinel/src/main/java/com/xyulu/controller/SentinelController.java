package com.xyulu.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author luming
 * @version 1.0
 */
@RestController
public class SentinelController {

    @GetMapping("/sentinel/test")
    public String test() {
        return "sentinel test ";
    }

    @GetMapping("/sentinel/test1")
    public String test1() {
        return "sentinel test1 ";
    }
}