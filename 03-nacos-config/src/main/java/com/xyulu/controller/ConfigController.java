package com.xyulu.controller;

import com.xyulu.config.DateFormatConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author luming
 * @version 1.0
 * @date
 */
@RestController
//@RefreshScope
public class ConfigController {

//    @Value("${pattern.dateformat}")
//    private String dateformat;

    @Autowired
    private DateFormatConfig dateformat;

    @GetMapping("/now")
    public String time() {
        String result = dateformat.getDateformat() +"=========" + dateformat.getShareContent();
        return result;
    }

}
