package com.xyulu.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author luming
 * @version 1.0
 * @date
 */
@Data
@Component
@ConfigurationProperties(prefix = "pattern")
public class DateFormatConfig {

    private String dateformat;

    private String shareContent;
}
