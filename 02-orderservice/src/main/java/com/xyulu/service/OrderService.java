package com.xyulu.service;

import com.xyulu.mapper.OrderMapper;
import com.xyulu.pojo.Order;
import com.xyulu.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OrderService {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private RestTemplate restTemplate;

    public Order queryOrderById(Long orderId) {
        // 1.查询订单
        Order order = orderMapper.findById(orderId);

        String url = "http://userservice/user/" + order.getUserId();
        User orderUser = restTemplate.getForObject(url, User.class);

        order.setUser(orderUser);
        // 4.返回
        return order;
    }
}
